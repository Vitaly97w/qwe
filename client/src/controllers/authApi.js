import axios from 'axios'
import jwt from 'jsonwebtoken'
import router from '../router/index'
const url = 'http://localhost:5000/api/auth/login'

export default class API {
  //Добавление в базу данных
  static loginUser(user) {
    axios.post(url, user).then((req) => {
        localStorage.setItem('token', req.data.token)
        let user = jwt.decode(req.data.token.replace('Bearer ', ''))
        localStorage.setItem('roles', user.roles)

        if (user.roles.includes('MODERATOR')) {
          router.push({ name: 'HomePageM' })
        } else if (user.roles.includes('EMPLOYEE')) {
          router.push({ name: 'HomePageHR' })
        }
    }).catch((e)=>{
      alert("Неверное имя пользователя или пароль!")
    })
  }
}
